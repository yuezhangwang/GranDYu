# GranDYu

#### 项目介绍
GranDYu是一个自定义，为C++加入一些（*并不*）实用的语法糖的小工具包

#### 安装教程
将include文件夹内的头文件加入项目中，包含之

##### 建议项目属性（可选）
本工具包更倾向于在Debug模式下发挥作用，所以为了更好的调试体验，建议打开Visual Studio的`/FC`编译器选项，如下图设置：

![Compiler options:/FC](http://omjn1u0j6.bkt.clouddn.com/18-9-21/75906607.jpg)

#### 使用说明

目前只有THROW_IF_FAILED宏，请看example文件夹例子