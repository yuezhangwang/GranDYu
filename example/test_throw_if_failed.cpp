// [test_throw_if_failed.cpp] Test THROW_IF_FAILED macro
#include <iostream>
#include <windows.h>	// 测试HRESULT类型和FAILED宏
using namespace std;

// step 1:
// 先包含相应头文件
#include "throw_if_failed.hpp"

// step 2:
// 然后注册要检测的函数的返回值类型与对应谓词
// 其中谓词要求为：当返回“失败”时，结果为真，例如：FAILED(hr)
TRACE_RETURN_TYPE(HRESULT, FAILED)

// step 3 (optional):
// 自定义谓词
struct IntFailed
{
	constexpr bool operator()(int res) const
	{
		return res < 0;
	}
};

inline bool Pre(int d) { return d < 0; }

// 支持同时不同返回值类型函数
TRACE_RETURN_TYPE(int, IntFailed())
//TRACE_RETURN_TYPE(int, Pre)				同样OK，但同一类型只能注册一个谓词

HRESULT TestMoc();
int TestMoc(int d);

int main()
{
	try
	{
		THROW_IF_FAILED TestMoc();
		THROW_IF_FAILED TestMoc(666);
	}
	catch (exception e)
	{
		cout << e.what() << endl;
	}
}

HRESULT TestMoc()
{
	return static_cast<HRESULT>(-1);
}

int TestMoc(int d)
{
	return -1;
}
